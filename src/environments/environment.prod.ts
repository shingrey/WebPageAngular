export const environment = {
  production: true,
  media: 'https://unprogramador.com/wp-json/wp/v2/media/',
  posts: 'https://unprogramador.com/wp-json/wp/v2/posts?per_page=12&author=1',
  yoUp: 'https://unprogramador.com/author/admin/page/2/'

};
