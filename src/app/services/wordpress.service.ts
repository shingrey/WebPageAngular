import { Injectable } from '@angular/core';
import {Wordpress} from '../models/wordpress'
import {WordpressImg} from '../models/wordpressImg';
import { Observable ,  of } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {environment} from '../../environments/environment';

@Injectable()
export class WordpressService {
  mapImg = new Map<number,string>();

  constructor(private http: HttpClient) { }
  getPostWordpress(): Observable<Wordpress[]>{
    
    return this.http.get<Wordpress[]>(environment.posts);
  }
  getListMedia(wp: any):Observable<any>{
    wp.forEach(e => {
      this.http.get<WordpressImg>(environment.media + e.featured_media).subscribe(
        resp => this.mapImg.set(e.featured_media, resp.source_url));
    });
    return of(this.mapImg);  
    }

}
