import { Injectable } from '@angular/core';
import {ContactForm} from '../models/contactForm';
import { Observable ,  of } from 'rxjs';
import {HttpClient} from '@angular/common/http';





@Injectable()
export class SendEmailService {
  type:any;
  constructor( private http: HttpClient) { }
  getResponseEmail(_body: ContactForm): Observable<any>{
    // this.http.post('/send.php',_body).subscribe(data =>{
    //   console.log(data);
    //   this.type = JSON.stringify(data);
    // });
    return this.http.post('/send.php',_body);
  }

}
