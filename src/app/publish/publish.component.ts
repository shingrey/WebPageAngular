import { Component, OnInit } from '@angular/core';
import {ContactForm} from '../models/contactForm';
import {WordpressService} from '../services/wordpress.service';
import { Observable ,  of } from 'rxjs';
import {Wordpress} from '../models/wordpress';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-publish',
  templateUrl: './publish.component.html',
  styleUrls: ['./publish.component.css']
})
export class PublishComponent implements OnInit {
  infoWordpress:Array<Wordpress>;
  imgs: Array<string> ;
  yoUrl:string = environment.yoUp;
  mapImg = new Map<number,string>();



  constructor(private wServices:WordpressService) {
    
   }

  ngOnInit() {
    this.getPost();
  }
  getPost(){
    this.wServices.getPostWordpress().subscribe(
      result => {
        this.infoWordpress = result;
        this.getMedia(result);
    },
  err=>{
  });
  
  }
  getMedia(info:any){
    this.wServices.getListMedia(info).subscribe(dat => this.mapImg = dat);
  }

}
