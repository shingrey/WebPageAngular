import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'quitarSlash'
})
export class QuitarSlashPipe implements PipeTransform {

  transform(value: string): any {
    return value.replace('&#8211;',' - ');
  }

}
