import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'quitarTag'
})
export class QuitarTagPipe implements PipeTransform {

  transform(value: string): any {
    let tag1 = value.replace('<p>','');
    let final = tag1.replace('</p>','');
    return final;
  }

}
